import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

public class Work1_1 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;

    public Work1_1() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ramen");
            }
        });
        udonButton.addComponentListener(new ComponentAdapter() {
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("udon");
            }
        });

    }
    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Do you want to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0) {
            textPane1.setText("Thank you for ordering " + food);
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+ food + "! It will be served as soon as possible.");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Work1_1");
        frame.setContentPane(new Work1_1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
